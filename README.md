# AutoTechDraw

This project is a prototype of using FreeCAD+Python for automatic generation of technical drawings.

Python 3.8.10 (default, May 26 2023, 14:05:08) 
[GCC 9.4.0] on linux
>>> import cairosvg
>>> import Part
>>> import os, sys
>>> from pathlib import Path
>>> sys.path.append("/home/quaoar/work/sheet-metal-drawings")
>>> os.chdir("/home/quaoar/work/sheet-metal-drawings")
>>> from app.modules.process_part import PartProcessing
>>> from tests.regression.helpers import find_first_solid
>>> test_dir = Path("/home/quaoar/work/sheet-metal-drawings/regression/data")
>>> input_path = "/home/quaoar/work/sheet-metal-drawings/tests/regression/data/4warnings_0-1-1-1_1_flat-pattern.brep"
>>> title_json = {
... "notes_text": "Deburred",
... "material_name": "",
... "part_quantity": 0,
... "part_weight": 0.0,
... "drawing_data": "",
... "part_id": "1",
... "order_number": ""
... }
>>> inspected_step_path = Path("/home/quaoar/work/sheet-metal-drawings/tests/regression/data/4warnings_0-1-1-1_1.stp")
>>> f = open("/home/quaoar/work/sheet-metal-drawings/tests/regression/data/4warnings.json")
>>> import json
>>> body_json = json.load(f)
>>> part = PartProcessing(inspected_step_path,input_path,body_json,title_json,Path("/home/quaoar/work/outcome"),False,False)
>>> part.make_drawing()
2.0
2.0
(PosixPath('/home/quaoar/work/outcome.pdf'), ['2:1', '2:1'])
>>> 

import importlib
importlib.reload(autotd.modules.api)
import autotd.modules.api
utils = autotd.modules.api.PartUtils()
